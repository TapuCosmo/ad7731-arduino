# AD7731
Arduino library for the Analog Devices AD7731 ADC chip

Forked from https://github.com/wdconinc/AD7731.

# Quickstart
To load, include the header:
 #include "AD7731.h"
Create AD7731 objects:
 AD7731 adc();
Call functions on AD7731 object:
 int read = adc.getData();

# Different modes of operation
