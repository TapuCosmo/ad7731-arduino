/*
Copyright (C) 2016 Wouter Deconinck
Copyright (C) 2020 Vietbao Tran

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* AD7731

Arduino library for the Analog Device AD7731 6-channel 24-bit ADC chip

Info: http://www.analog.com/en/products/analog-to-digital-converters/ad-converters/ad7731.html
Datasheet: http://www.analog.com/media/en/technical-documentation/data-sheets/AD7731.pdf

Authors:      Wouter Deconinck, Vietbao Tran
Contributors: Lisa Oswald

*/

#ifndef AD7731_h
#define AD7731_h

#include "Arduino.h"
#include "SPI.h"

#define AD7731_COMM_READ_REGISTER_STATUS_SINGLE 0x10
#define AD7731_COMM_READ_REGISTER_STATUS_START_CONT 0x20
#define AD7731_COMM_READ_REGISTER_STATUS_STOP_CONT 0x30

#define AD7731_COMM_READ_REGISTER_DATA_SINGLE 0x11
#define AD7731_COMM_READ_REGISTER_DATA_START_CONT 0x21
#define AD7731_COMM_READ_REGISTER_DATA_STOP_CONT 0x31

#define AD7731_COMM_READ_REGISTER_MODE_SINGLE 0x12
#define AD7731_COMM_READ_REGISTER_MODE_START_CONT 0x22
#define AD7731_COMM_READ_REGISTER_MODE_STOP_CONT 0x32

#define AD7731_COMM_READ_REGISTER_FILTER_SINGLE 0x13
#define AD7731_COMM_READ_REGISTER_OFFSET_SINGLE 0x15
#define AD7731_COMM_READ_REGISTER_GAIN_SINGLE 0x16

#define AD7731_COMM_WRITE_REGISTER_COMM_Y 0x00
#define AD7731_COMM_WRITE_REGISTER_MODE_Y 0x02
#define AD7731_COMM_WRITE_REGISTER_FILTER_Y 0x03
#define AD7731_COMM_WRITE_REGISTER_OFFSET_Y 0x05
#define AD7731_COMM_WRITE_REGISTER_GAIN_Y 0x06

#define AD7731_STATUS_MASK_READY 0x80
#define AD7731_STATUS_MASK_STDY  0x40
#define AD7731_STATUS_MASK_STBY  0x20
#define AD7731_STATUS_MASK_NOREF 0x10

class AD7731 {

  public:

    AD7731();
    ~AD7731();

    // Mode Register

    // Set the operating mode
    #define AD7731_OPERATING_MODE_MASK 0xE000
    enum class OPERATING_MODE : unsigned int {
      IDLE = 0x0000,
      CONTINUOUS_CONVERSION = 0x2000,
      SINGLE_CONVERSION = 0x4000,
      STANDBY = 0x6000,
      INTERNAL_ZERO_SCALE_CALIBRATION = 0x8000,
      INTERNAL_FULL_SCALE_CALIBRATION = 0xA000,
      SYSTEM_ZERO_SCALE_CALIBRATION = 0xC000,
      SYSTEM_FULL_SCALE_CALIBRATION = 0xE000
    };
    void setOperatingMode(const OPERATING_MODE m) {
      setRegisterBits(registerMode, (unsigned int)m, AD7731_OPERATING_MODE_MASK);
      writeRegisterMode();
    }

    // Set the input range
    #define AD7731_MODE_RANGE_MASK 0x0070
    enum class MODE_RANGE : unsigned int {
      RANGE_10mV   = 0x0000,
      RANGE_20mV   = 0x0010,
      RANGE_40mV   = 0x0020,
      RANGE_80mV   = 0x0030,
      RANGE_160mV  = 0x0040,
      RANGE_320mV  = 0x0050,
      RANGE_640mV  = 0x0060,
      RANGE_1280mV = 0x0070
    };
    void setRange(const MODE_RANGE r) {
      setRegisterBits(registerMode, (unsigned int)r, AD7731_MODE_RANGE_MASK);
      writeRegisterMode();
    }

    // Set the polarity (bipolar v. unipolar)
    #define AD7731_MODE_POLARITY_MASK 0x1000
    enum class MODE_POLARITY : unsigned int {
      BIPOLAR  = 0x0000,
      UNIPOLAR = 0x1000
    };
    void setPolarity(const MODE_POLARITY p) {
      setRegisterBits(registerMode, (unsigned int)p, AD7731_MODE_POLARITY_MASK);
      writeRegisterMode();
    }
    const MODE_POLARITY getPolarity() {
      return (MODE_POLARITY)(readRegisterMode() & AD7731_MODE_POLARITY_MASK);
    }

    #define AD7731_MODE_CHANNEL_MASK 0x0007
    enum class MODE_CHANNEL : unsigned int {
      PSEUDO_DIFF_AIN16 = 0x0000,
      PSEUDO_DIFF_AIN26 = 0x0001,
      PSEUDO_DIFF_AIN36 = 0x0002,
      PSEUDO_DIFF_AIN46 = 0x0003,
      FULLY_DIFF_AIN12  = 0x0004,
      FULLY_DIFF_AIN34  = 0x0005,
      FULLY_DIFF_AIN56  = 0x0006,
      TEST              = 0x0007
    };
    void setChannelMode(const MODE_CHANNEL c) {
      setRegisterBits(registerMode, (unsigned int)c, AD7731_MODE_CHANNEL_MASK);
      writeRegisterMode();
    }

    #define AD7731_MODE_DOUT_MASK 0x0f00
    enum class MODE_DOUT : unsigned int {
      DISABLE = 0x0000,
      ENABLE  = 0x0800,
      D1      = 0x0400,
      D0      = 0x0200
    };
    void setDigitalOut(const MODE_DOUT d) {
      setRegisterBits(registerMode, (unsigned int)d, AD7731_MODE_DOUT_MASK);
      writeRegisterMode();
    }

    #define AD7731_MODE_WORDLENGTH_MASK 0x0100
    enum class MODE_WORDLENGTH : unsigned int {
      WL_16BITS = 0x0000,
      WL_24BITS = 0x0100
    };
    void setWordLength(const MODE_WORDLENGTH wl) {
      setRegisterBits(registerMode, (unsigned int)wl, AD7731_MODE_WORDLENGTH_MASK);
      writeRegisterMode();
    }
    unsigned int getWordLength() {
      return registerMode & AD7731_MODE_WORDLENGTH_MASK;
    }

    #define AD7731_MODE_HIREF_MASK 0x0080
    enum class MODE_HIREF : unsigned int {
      ENABLE = 0x0080,
      DISABLE = 0x0000
    };
    void setHiRef(const MODE_HIREF h) {
      setRegisterBits(registerMode, (unsigned int)h, AD7731_MODE_HIREF_MASK);
      writeRegisterMode();
    }

    // Filter Register

    #define AD7731_FILTER_FAST_MASK 0x01
    enum class FILTER_FAST : unsigned int {
      ENABLE  = 0x01,
      DISABLE = 0x00
    };
    void setFilterFast(const FILTER_FAST f) {
      setRegisterBits(registerFilter, (unsigned int)f, AD7731_FILTER_FAST_MASK);
      writeRegisterFilter();
    }

    #define AD7731_FILTER_SKIP_MASK 0x02
    enum class FILTER_SKIP : unsigned int {
      ENABLE  = 0x01,
      DISABLE = 0x00
    };
    void setFilterSkip(const FILTER_SKIP s) {
      setRegisterBits(registerFilter, (unsigned int)s, AD7731_FILTER_SKIP_MASK);
      writeRegisterFilter();
    }

    #define AD7731_FILTER_CHP_MASK 0x04
    enum class FILTER_CHP : unsigned int {
      ENABLE  = 0x01,
      DISABLE = 0x00
    };
    void setFilterChp(const FILTER_CHP c) {
      setRegisterBits(registerFilter, (unsigned int)c, AD7731_FILTER_CHP_MASK);
      writeRegisterFilter();
    }

    // Reset the chip
    void reset() {
      // Writing 4 times 8 ones resets the chip
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
    }

    // Read ADC value
    unsigned long readData() {
      return readRegisterData();
    }

    // Write mode register
    void printRegisterMode(Stream &stream) {
      unsigned int reg = readRegisterMode();
      stream.print("AD7731: mode ");
      stream.println(reg, HEX);
    }

    // Write gain register
    void printRegisterGain(Stream &stream) {
      unsigned int reg = readRegisterGain();
      stream.print("AD7731: gain ");
      stream.println(reg, HEX);
    }

    // Write filter register
    void printRegisterFilter(Stream &stream) {
      unsigned long reg = readRegisterFilter();
      stream.print("AD7731: filter ");
      stream.println(reg, HEX);
    }

    // Write offset register
    void printRegisterOffset(Stream &stream) {
      unsigned long reg = readRegisterOffset();
      stream.print("AD7731: offset ");
      stream.println(reg, HEX);
    }

    // Write data register
    void printRegisterData(Stream &stream) {
      unsigned long reg = readRegisterData();
      stream.print("AD7731: data ");
      stream.println(reg, HEX);
    }

    // External RDY Pin
    void attachReadyPin(unsigned int pin) {
      rdyPin = pin;
      pinMode(pin, INPUT_PULLUP);
    }

    bool getReadyState() {
      if (rdyPin >= 0) {
        return !digitalRead(rdyPin);
      }
    }

  private:

    void writeRegisterOffset() {
      SPI.transfer(AD7731_COMM_WRITE_REGISTER_OFFSET_Y);
      SPI.transfer((registerOffset & 0xff0000) >> 16);
      SPI.transfer((registerOffset & 0x00ff00) >> 8);
      SPI.transfer((registerOffset & 0x0000ff) >> 0);
    }

    void writeRegisterFilter() {
      SPI.transfer(AD7731_COMM_WRITE_REGISTER_FILTER_Y);
      SPI.transfer((registerFilter & 0xff0000) >> 16);
      SPI.transfer((registerFilter & 0x00ff00) >> 8);
      SPI.transfer((registerFilter & 0x0000ff) >> 0);
    }

    void writeRegisterGain() {
      SPI.transfer(AD7731_COMM_WRITE_REGISTER_GAIN_Y);
      SPI.transfer((registerGain & 0xff00) >> 8);
      SPI.transfer((registerGain & 0x00ff) >> 0);
    }

    void writeRegisterMode() {
      SPI.transfer(AD7731_COMM_WRITE_REGISTER_MODE_Y);
      SPI.transfer((registerMode & 0xff00) >> 8);
      SPI.transfer((registerMode & 0x00ff) >> 0);
    }

    unsigned long readRegisterOffset() {
      SPI.transfer(AD7731_COMM_READ_REGISTER_OFFSET_SINGLE);
      return readBytes(3) & 0xffffff;
    }

    unsigned long readRegisterFilter() {
      SPI.transfer(AD7731_COMM_READ_REGISTER_FILTER_SINGLE);
      return readBytes(3) & 0xffffff;
    }

    unsigned long readRegisterGain() {
      SPI.transfer(AD7731_COMM_READ_REGISTER_GAIN_SINGLE);
      return readBytes(2) & 0xffff;
    }

    unsigned long readRegisterMode() {
      SPI.transfer(AD7731_COMM_READ_REGISTER_MODE_SINGLE);
      return readBytes(2) & 0xffff;
    }

    unsigned long readRegisterData() {
      if (!registerDataContRead) {
        SPI.transfer(AD7731_COMM_READ_REGISTER_DATA_SINGLE);
      }
      const unsigned int wordLength = getWordLength();
      if (wordLength == (unsigned int)MODE_WORDLENGTH::WL_16BITS) {
        return readBytes(2) & 0xffff;
      } else if (wordLength == (unsigned int)MODE_WORDLENGTH::WL_24BITS) {
        return readBytes(3) & 0xffffff;
      }
    }

    void writeRegisterDataStartContRead() {
      registerDataContRead = true;
      SPI.transfer(AD7731_COMM_READ_REGISTER_DATA_START_CONT);
    }

    void writeRegisterDataStopContRead() {
      registerDataContRead = false;
      SPI.transfer(AD7731_COMM_READ_REGISTER_DATA_STOP_CONT);
    }

    // Set register bits with bits and mask
    void setRegisterBits(unsigned long & reg, unsigned int bits, unsigned int mask) {
      reg = (reg & ~mask) | (bits & mask);
    }

    // Read a specified number of bytes from SPI (n = 1,2,3,4)
    unsigned long readBytes(unsigned int n) {
      unsigned long value = 0;
      for (unsigned int i = 0; i < n; i++) {
        value <<= 8;
        value |= SPI.transfer(0);
      }
      return value;
    }

    // Registers for writing (local copy)
    unsigned long registerMode; // 16 bits
    unsigned long registerGain; // 16 bits
    unsigned long registerFilter; // 24 bits
    unsigned long registerOffset; // 24 bits

    // Continuous Read States
    bool registerDataContRead = false;

    // RDY Pin
    unsigned int rdyPin;
};

#endif // AD7731
